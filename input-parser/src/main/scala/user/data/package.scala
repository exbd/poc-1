package user

import org.apache.spark.sql.types._

package object data {
  lazy val SK_ID_CURR = "SK_ID_CURR"
  lazy val TARGET = "TARGET"
  lazy val NAME_CONTRACT_TYPE = "NAME_CONTRACT_TYPE"
  lazy val CODE_GENDER = "CODE_GENDER"
  lazy val FLAG_OWN_CAR = "FLAG_OWN_CAR"
  lazy val FLAG_OWN_REALTY = "FLAG_OWN_REALTY"
  lazy val CNT_CHILDREN = "CNT_CHILDREN"
  lazy val AMT_INCOME_TOTAL = "AMT_INCOME_TOTAL"
  lazy val AMT_CREDIT = "AMT_CREDIT"
  lazy val AMT_ANNUITY = "AMT_ANNUITY"
  lazy val AMT_GOODS_PRICE = "AMT_GOODS_PRICE"
  lazy val NAME_TYPE_SUITE = "NAME_TYPE_SUITE"
  lazy val NAME_INCOME_TYPE = "NAME_INCOME_TYPE"
  lazy val NAME_EDUCATION_TYPE = "NAME_EDUCATION_TYPE"
  lazy val NAME_FAMILY_STATUS = "NAME_FAMILY_STATUS"
  lazy val NAME_HOUSING_TYPE = "NAME_HOUSING_TYPE"
  lazy val REGION_POPULATION_RELATIVE = "REGION_POPULATION_RELATIVE"
  lazy val DAYS_BIRTH = "DAYS_BIRTH"
  lazy val DAYS_EMPLOYED = "DAYS_EMPLOYED"
  lazy val DAYS_REGISTRATION = "DAYS_REGISTRATION"
  lazy val DAYS_ID_PUBLISH = "DAYS_ID_PUBLISH"
  lazy val OWN_CAR_AGE = "OWN_CAR_AGE"
  lazy val FLAG_MOBIL = "FLAG_MOBIL"
  lazy val FLAG_EMP_PHONE = "FLAG_EMP_PHONE"
  lazy val FLAG_WORK_PHONE = "FLAG_WORK_PHONE"
  lazy val FLAG_CONT_MOBILE = "FLAG_CONT_MOBILE"
  lazy val FLAG_PHONE = "FLAG_PHONE"
  lazy val FLAG_EMAIL = "FLAG_EMAIL"
  lazy val OCCUPATION_TYPE = "OCCUPATION_TYPE"

  val schema = StructType(
    StructField(SK_ID_CURR, IntegerType) ::
      StructField(TARGET, IntegerType) ::
      StructField(NAME_CONTRACT_TYPE, StringType) ::
      StructField(CODE_GENDER, StringType) ::
      StructField(FLAG_OWN_CAR, StringType) ::
      StructField(FLAG_OWN_REALTY, StringType) ::
      StructField(CNT_CHILDREN, IntegerType) ::
      StructField(AMT_INCOME_TOTAL, DoubleType) ::
      StructField(AMT_CREDIT, DoubleType) ::
      StructField(AMT_ANNUITY, DoubleType) ::
      StructField(AMT_GOODS_PRICE, DoubleType) ::
      StructField(NAME_TYPE_SUITE, StringType) ::
      StructField(NAME_INCOME_TYPE, StringType) ::
      StructField(NAME_EDUCATION_TYPE, StringType) ::
      StructField(NAME_FAMILY_STATUS, StringType) ::
      StructField(NAME_HOUSING_TYPE, StringType) ::
      StructField(REGION_POPULATION_RELATIVE, DoubleType) ::
      StructField(DAYS_BIRTH, IntegerType) ::
      StructField(DAYS_EMPLOYED, IntegerType) ::
      StructField(DAYS_REGISTRATION, DoubleType) ::
      StructField(DAYS_ID_PUBLISH, IntegerType) ::
      StructField(OWN_CAR_AGE, StringType) ::
      StructField(FLAG_MOBIL, IntegerType) ::
      StructField(FLAG_EMP_PHONE, IntegerType) ::
      StructField(FLAG_WORK_PHONE, IntegerType) ::
      StructField(FLAG_CONT_MOBILE, IntegerType) ::
      StructField(FLAG_PHONE, IntegerType) ::
      StructField(FLAG_EMAIL, IntegerType) ::
      StructField(OCCUPATION_TYPE, StringType) ::
      Nil)
}
