package config

import com.typesafe.config.ConfigFactory

object Settings {
  private val config = ConfigFactory.load()

  object CreditCardBalanceConfig {
    private val inputParserConfig = config.getConfig("inputParser")

    // directories
    lazy val creditCardBalanceInputDir: String = inputParserConfig.getString("creditCardBalanceInputDir")
    lazy val installmentPaymentsInputDir: String = inputParserConfig.getString("installmentPaymentsInputDir")
    lazy val applicationsInputDir: String = inputParserConfig.getString("applicationsInputDir")
    lazy val applicationsRawOutputDir: String = inputParserConfig.getString("applicationsRawOutputDir")

    // kafka
    lazy val kafkaBootstrapServers: String = inputParserConfig.getString("kafkaBootstrapServers")

    // env
    lazy val master: String = inputParserConfig.getString("master")
  }

}
