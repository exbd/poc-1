package credit.card

import org.apache.spark.sql.types._

package object balance {
  val SK_ID_PREV = "SK_ID_PREV"
  val SK_ID_CURR = "SK_ID_CURR"
  val MONTHS_BALANCE = "MONTHS_BALANCE"
  val AMT_BALANCE = "AMT_BALANCE"
  val AMT_CREDIT_LIMIT_ACTUAL = "AMT_CREDIT_LIMIT_ACTUAL"
  val AMT_DRAWINGS_ATM_CURRENT = "AMT_DRAWINGS_ATM_CURRENT"
  val AMT_DRAWINGS_CURRENT = "AMT_DRAWINGS_CURRENT"
  val AMT_DRAWINGS_OTHER_CURRENT = "AMT_DRAWINGS_OTHER_CURRENT"
  val AMT_DRAWINGS_POS_CURRENT = "AMT_DRAWINGS_POS_CURRENT"
  val AMT_INST_MIN_REGULARITY = "AMT_INST_MIN_REGULARITY"
  val AMT_PAYMENT_CURRENT = "AMT_PAYMENT_CURRENT"
  val AMT_PAYMENT_TOTAL_CURRENT = "AMT_PAYMENT_TOTAL_CURRENT"
  val AMT_RECEIVABLE_PRINCIPAL = "AMT_RECEIVABLE_PRINCIPAL"
  val AMT_RECIVABLE = "AMT_RECIVABLE"
  val AMT_TOTAL_RECEIVABLE = "AMT_TOTAL_RECEIVABLE"
  val CNT_DRAWINGS_ATM_CURRENT = "CNT_DRAWINGS_ATM_CURRENT"
  val CNT_DRAWINGS_CURRENT = "CNT_DRAWINGS_CURRENT"
  val CNT_DRAWINGS_OTHER_CURRENT = "CNT_DRAWINGS_OTHER_CURRENT"
  val CNT_DRAWINGS_POS_CURRENT = "CNT_DRAWINGS_POS_CURRENT"
  val CNT_INSTALMENT_MATURE_CUM = "CNT_INSTALMENT_MATURE_CUM"
  val NAME_CONTRACT_STATUS = "NAME_CONTRACT_STATUS"
  val SK_DPD = "SK_DPD"
  val SK_DPD_DEF = "SK_DPD_DEF"

  val schema = StructType(
    StructField(SK_ID_PREV, IntegerType) ::
      StructField(SK_ID_CURR, IntegerType) ::
      StructField(MONTHS_BALANCE, IntegerType) ::
      StructField(AMT_BALANCE, DoubleType) ::
      StructField(AMT_CREDIT_LIMIT_ACTUAL, IntegerType) ::
      StructField(AMT_DRAWINGS_ATM_CURRENT, DoubleType) ::
      StructField(AMT_DRAWINGS_CURRENT, DoubleType) ::
      StructField(AMT_DRAWINGS_OTHER_CURRENT, DoubleType) ::
      StructField(AMT_DRAWINGS_POS_CURRENT, DoubleType) ::
      StructField(AMT_INST_MIN_REGULARITY, DoubleType) ::
      StructField(AMT_PAYMENT_CURRENT, DoubleType) ::
      StructField(AMT_PAYMENT_TOTAL_CURRENT, DoubleType) ::
      StructField(AMT_RECEIVABLE_PRINCIPAL, DoubleType) ::
      StructField(AMT_RECIVABLE, DoubleType) ::
      StructField(AMT_TOTAL_RECEIVABLE, DoubleType) ::
      StructField(CNT_DRAWINGS_ATM_CURRENT, DoubleType) ::
      StructField(CNT_DRAWINGS_CURRENT, IntegerType) ::
      StructField(CNT_DRAWINGS_OTHER_CURRENT, DoubleType) ::
      StructField(CNT_DRAWINGS_POS_CURRENT, DoubleType) ::
      StructField(CNT_INSTALMENT_MATURE_CUM, DoubleType) ::
      StructField(NAME_CONTRACT_STATUS, StringType) ::
      StructField(SK_DPD, IntegerType) ::
      StructField(SK_DPD_DEF, IntegerType) :: Nil)
}
