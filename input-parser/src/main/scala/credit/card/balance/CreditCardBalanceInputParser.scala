package credit.card.balance

import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import utils._

object CreditCardBalanceInputParser {
  def main(args: Array[String]): Unit = {
    disableLogging()

    val input = getInput("input-parser-credit-card-balance", config.creditCardBalanceInputDir, schema)
    val parsed = parseInput(input)
    val kafkaData = prepareDataForKafka(parsed)
    val writer = createKafkaWriter(kafkaData, "credit_card_balance_topic")

    startWriter(writer)
  }

  def parseInput(input: DataFrame): Dataset[(String, String)] = {
    val maxMonthsBalanceColumnName = "maxMonthsBalance"

    input
      .select(col(SK_ID_CURR), struct(col(MONTHS_BALANCE), col(AMT_BALANCE)).alias("balance"))
      .groupBy(SK_ID_CURR)
      .agg(max("balance").alias(maxMonthsBalanceColumnName))
      .select(col(SK_ID_CURR), col(s"$maxMonthsBalanceColumnName.$AMT_BALANCE"))
      .map(row => (row(0).toString
        , toJson(Map[String, String]("id" -> row(0).toString, "balance" -> row(1).toString))))(Encoders.product[(String, String)])
  }
}
