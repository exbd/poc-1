package utils

import java.io.{BufferedReader, File, FileReader}

import org.apache.spark.sql.types.{IntegerType, StructType}

object Codegen {
  val SK_ID_PREV = "SK_ID_PREV"

  private def getStructType(s: String): String = {
    try {
      s.toInt
      "IntegerType"
    } catch {
      case _: Exception =>
        try {
          s.toDouble
          "DoubleType"
        } catch {
          case _: Exception => "StringType"
        }
    }
  }

  def main(args: Array[String]): Unit = {
    val file = new File("C:\\Hadoop\\BankDataSet\\application_train.csv")
    try {
      val br = new BufferedReader(new FileReader(file))
      val header = br.readLine()
      val headerSplits = header.split(",")
      val firstLine = br.readLine()
      val firstLineSplits = firstLine.split(",")

      for (split <- headerSplits) {
        println(s"val $split = ${'"'}$split${'"'}")
      }

      println()
      println("val schema = StructType(")

      for (i <- headerSplits.indices) {
        val headerSplit = headerSplits(i)
        val lineSplit = firstLineSplits(i)
        val structType = getStructType(lineSplit)

        println(s"StructField($headerSplit, $structType) ::")
      }
      print("Nil")
      println(")")
    } catch {
      case e: Exception => e.printStackTrace()
    }
  }
}
