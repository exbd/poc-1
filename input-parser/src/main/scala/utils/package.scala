import java.util.Properties

import com.google.gson.JsonObject
import config.Settings
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig}
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.streaming.{DataStreamWriter, OutputMode, StreamingQuery}
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import org.apache.spark.streaming.{Duration, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}

package object utils {
  val config: Settings.CreditCardBalanceConfig.type = Settings.CreditCardBalanceConfig

  def getSparkContext(appName: String): SparkContext = {
    SparkContext.getOrCreate(getSparkConf(appName))
  }

  def getSparkSession(appName: String): SparkSession = {
    SparkSession
      .builder()
      .config(getSparkConf(appName))
      .getOrCreate()
  }

  private def getSparkConf(appName: String): SparkConf = {
    new SparkConf()
      .setAppName(appName)
      .setMaster(config.master)
      .set("spark.sql.streaming.checkpointLocation", s"$appName-checkpoint")
  }

  def getStreamingContext(streamingApp: (SparkContext, Duration) => StreamingContext, sc: SparkContext, batchDuration: Duration): StreamingContext = {
    val creatingFunc = () => streamingApp(sc, batchDuration)
    val ssc = sc.getCheckpointDir match {
      case Some(checkpointDir) => StreamingContext.getActiveOrCreate(checkpointDir, creatingFunc, sc.hadoopConfiguration, createOnError = true)
      case None => StreamingContext.getActiveOrCreate(creatingFunc)
    }
    sc.getCheckpointDir.foreach(cp => ssc.sparkContext.setCheckpointDir(cp))
    ssc
  }

  def getKafkaProducer: KafkaProducer[String, String] = {
    val properties = new Properties()
    properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, config.kafkaBootstrapServers)
    properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer].getName)
    properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer].getName)
    // robustness
    properties.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true")
    properties.put(ProducerConfig.ACKS_CONFIG, "all")
    properties.put(ProducerConfig.RETRIES_CONFIG, Integer.toString(Integer.MAX_VALUE))
    properties.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "5")

    // performance
    properties.put(ProducerConfig.COMPRESSION_TYPE_CONFIG, "snappy")
    properties.put(ProducerConfig.LINGER_MS_CONFIG, "20")
    properties.put(ProducerConfig.BATCH_SIZE_CONFIG, Integer.toString(32 * 1024)) // 32kb

    new KafkaProducer[String, String](properties)
  }

  def createKafkaWriter(data: DataFrame, topic: String, outputMode: OutputMode = OutputMode.Update): DataStreamWriter[Row] = {
    data
      .writeStream
      .format("kafka")
      .outputMode(outputMode)
      .option("kafka.bootstrap.servers", config.kafkaBootstrapServers)
      .option("topic", topic)
  }

  def prepareDataForKafka(data: Dataset[(String, String)]): DataFrame = {
    data.selectExpr("CAST (_1 AS STRING) AS key", "CAST (_2 AS STRING) AS value")
  }

  def startWriter(writer: DataStreamWriter[Row]): Unit = {
    writer.start().awaitTermination()
  }

  def toJson(data: Map[String, String]): String = {
    val json = new JsonObject()
    for (item <- data) {
      json.addProperty(item._1, item._2)
    }

    json.toString
  }

  def getInput(appName: String, inputDir: String, schema: StructType): DataFrame = {
    val spark = getSparkSession(appName)

    spark
      .readStream
      .option("header", value = true)
      .option("sep", ",")
      .schema(schema)
      .csv(inputDir)
  }

  def saveRawOutput(data: DataFrame, path: String, format: String = "csv"): StreamingQuery = {
    data
      .writeStream
      .format(format)
      .option("path", path)
      .start()
  }

  def disableLogging(): Unit = {
    Logger.getLogger("org").setLevel(Level.ERROR)
    Logger.getLogger("akka").setLevel(Level.ERROR)
  }
}
