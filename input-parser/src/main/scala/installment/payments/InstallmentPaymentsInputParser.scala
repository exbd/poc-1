package installment.payments

import org.apache.spark.sql.streaming.OutputMode
import org.apache.spark.sql.{DataFrame, Dataset}
import user.data._
import utils._

object InstallmentPaymentsInputParser {
  private val appName = "input-parser-installment-payments"

  def main(args: Array[String]): Unit = {
    disableLogging()

    val installmentPaymentsInput = getInput(appName, config.installmentPaymentsInputDir, schema)
      .select(SK_ID_CURR, AMT_INSTALMENT, AMT_PAYMENT, NUM_INSTALMENT_NUMBER)
      .repartition(16)

    val rawApplicationsInput = getInput(appName, config.applicationsInputDir, user.data.schema)

    saveRawOutput(rawApplicationsInput, config.applicationsRawOutputDir)

    val applicationsInput = rawApplicationsInput
      .select(SK_ID_CURR, NAME_INCOME_TYPE, OCCUPATION_TYPE, AMT_INCOME_TOTAL, NAME_HOUSING_TYPE)
      .repartition(16)

    val joined = installmentPaymentsInput.join(applicationsInput, SK_ID_CURR)
      .repartition(16)

    val parsed = parseInput(joined)
    val kafkaData = prepareDataForKafka(parsed)
    val writer = createKafkaWriter(kafkaData, "installment_payments_topic", OutputMode.Append)

    startWriter(writer)
  }

  def parseInput(input: DataFrame): Dataset[(String, String)] = {
    val spark = getSparkSession(appName)
    import spark.implicits._

    input
      .map(row => (row(0).toString, toJson(Map[String, String](elems =
        "id" -> row(0).toString,
        "installment_amount" -> getValue(row(1)),
        "payment_amount" -> getValue(row(2)),
        "installment_number" -> getValue(row(3)),
        "income_type" -> getValue(row(4)),
        "occupation_type" -> getValue(row(5)),
        "total_income" -> getValue(row(6)),
        "housing_type" -> getValue(row(7))
      ))))
  }

  def getValue(item: Any): String = {
    Option(item).getOrElse("").toString
  }
}
