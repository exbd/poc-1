package installment

import org.apache.spark.sql.types._

package object payments {
  val SK_ID_PREV ="SK_ID_PREV"
  val SK_ID_CURR ="SK_ID_CURR"
  val NUM_INSTALMENT_VERSION ="NUM_INSTALMENT_VERSION"
  val NUM_INSTALMENT_NUMBER ="NUM_INSTALMENT_NUMBER"
  val DAYS_INSTALMENT ="DAYS_INSTALMENT"
  val DAYS_ENTRY_PAYMENT ="DAYS_ENTRY_PAYMENT"
  val AMT_INSTALMENT ="AMT_INSTALMENT"
  val AMT_PAYMENT ="AMT_PAYMENT"

  val schema = StructType(
    StructField(SK_ID_PREV, IntegerType) ::
      StructField(SK_ID_CURR, IntegerType) ::
      StructField(NUM_INSTALMENT_VERSION, DoubleType) ::
      StructField(NUM_INSTALMENT_NUMBER, IntegerType) ::
      StructField(DAYS_INSTALMENT, DoubleType) ::
      StructField(DAYS_ENTRY_PAYMENT, DoubleType) ::
      StructField(AMT_INSTALMENT, DoubleType) ::
      StructField(AMT_PAYMENT, DoubleType) ::
      Nil)
}
