#!/usr/bin/env bash
spark-submit\
  --conf\
    "spark.driver.extraJavaOptions=-Dconfig.resource=prod.conf"\
  --conf\
    "spark.executor.extraJavaOptons=-Dconfig.resource=prod.conf"\
  --deploy-mode\
    cluster\
  --class\
    installment.payments.InstallmentPaymentsProcessor\
  --packages\
     org.apache.spark:spark-sql-kafka-0-10_2.11:2.3.2,com.typesafe:config:1.3.3\
  /home/hadoop/nzigic-jars/processor-1.0-SNAPSHOT-all.jar

