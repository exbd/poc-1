#!/usr/bin/env bash
hdfs dfs -mkdir /user/hadoop/nzigic/input/

hdfs dfs -mkdir /user/hadoop/nzigic/input/loan-applications
hdfs dfs -mkdir /user/hadoop/nzigic/input/installment-payments
hdfs dfs -mkdir /user/hadoop/nzigic/input/credit-card-balance

hdfs dfs -mkdir /user/hadoop/nzigic/raw/
hdfs dfs -mkdir /user/hadoop/nzigic/raw/loan-applications