#!/usr/bin/env bash
kafka-topics.sh --zookeeper localhost:2181 --create --topic spark_streaming_started_topic --replication-factor 1 --partitions 3
kafka-topics.sh --zookeeper localhost:2181 --create --topic spark_streaming_progress_topic --replication-factor 1 --partitions 3
kafka-topics.sh --zookeeper localhost:2181 --create --topic spark_streaming_termination_topic --replication-factor 1 --partitions 3

kafka-topics.sh --zookeeper localhost:2181 --create --topic installment_payments_topic --replication-factor 1 --partitions 3
kafka-topics.sh --zookeeper localhost:2181 --create --topic installment_payments_processed_topic --replication-factor 1 --partitions 3
kafka-topics.sh --zookeeper localhost:2181 --create --topic credit_card_balance_topics --replication-factor 1 --partitions 3

