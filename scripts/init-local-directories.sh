#!/usr/bin/env bash
cd ~
mkdir -p inputs/loan-applications
mkdir -p inputs/installment-payments
mkdir -p inputs/credit-card-balance

mkdir -p raw/loan-applications