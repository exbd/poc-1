#!/usr/bin/env bash
echo building shadow jars...

gradle -p ../ shadowJar

echo uploading jars to ws004...

scp -r ~/source/scala-poc-phase-1/processor/build/libs/. hadoop@ws004:/home/hadoop/nzigic-jars/

scp -r ~/source/scala-poc-phase-1/input-parser/build/libs/. hadoop@ws004:/home/hadoop/nzigic-jars/

scp -r ~/source/scala-poc-phase-1/storage-processed/build/libs/. hadoop@ws004:/home/hadoop/nzigic-jars/
