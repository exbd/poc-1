package config

import com.typesafe.config.ConfigFactory

object Settings {
  private val config = ConfigFactory.load()

  object StorageSettings {
    private val storageConfig = config.getConfig("storage")

    // env
    lazy val master: String = storageConfig.getString("master")

    // kafka
    lazy val kafkaBootstrapServers: String = storageConfig.getString("kafkaBootstrapServers")

    // cassandra
    lazy val cassandraHost: String = storageConfig.getString("cassandraHost")
    lazy val cassandraUsername: String = storageConfig.getString("cassandraUsername")
    lazy val cassandraPassword: String = storageConfig.getString("cassandraPassword")
  }

}
