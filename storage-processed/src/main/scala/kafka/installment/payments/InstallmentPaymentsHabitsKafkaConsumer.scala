package kafka.installment.payments

import com.datastax.spark.connector.AllColumns
import com.google.gson.JsonParser
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.KafkaUtils
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import utils.utils._
import com.datastax.spark.connector._
import org.joda.time.{DateTime, DateTimeZone}


object InstallmentPaymentsHabitsKafkaConsumer {
  def main(args: Array[String]): Unit = {
    disableLogging()

    val appName = "credit-card-balance-consumer"
    val sc = getSparkContext(appName)
    val ssc = new StreamingContext(sc, Seconds(1))
    ssc.checkpoint(s"$appName-checkpoint")

    KafkaUtils
      .createDirectStream[String, String](ssc, PreferConsistent, Subscribe[String, String](List("installment_payments_processed_topic").toSet, getConsumerProperties))
      .flatMap(t => parseMsg(t.value()))
      .foreachRDD(rdd => {
        if (!rdd.isEmpty)
          rdd.saveToCassandra("poc", "users_payment_habits", AllColumns)
      })

    ssc.start()
    ssc.awaitTermination()

  }

  def parseMsg(msg: String): Option[(String, Float, DateTime)] = {
    try {
      val jsonParser = new JsonParser()
      val json = jsonParser.parse(msg)
        .getAsJsonObject

      val occupationIncomeType = json.get("occupation_income_type").getAsString
      val paysOnTimePercentage = json.get("pays_on_time_percentage").getAsFloat

      Some((occupationIncomeType, paysOnTimePercentage, DateTime.now(DateTimeZone.UTC)))
    } catch {
      case e: Exception =>
        e.printStackTrace()
        None
    }
  }
}
