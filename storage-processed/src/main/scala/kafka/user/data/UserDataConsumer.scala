package kafka.user.data

import com.datastax.spark.connector._
import com.google.gson.JsonParser
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.{Seconds, StreamingContext}
import utils.utils._

object UserDataConsumer {
  def main(args: Array[String]): Unit = {
    disableLogging()

    val appName = "user-data-consumer"
    val sc = getSparkContext(appName)
    val ssc = new StreamingContext(sc, Seconds(1))
    ssc.checkpoint(s"$appName-checkpoint")

    val msgs = KafkaUtils
      .createDirectStream[String, String](ssc, PreferConsistent, Subscribe[String, String](List("user_data_topic").toSet, getConsumerProperties))
      .flatMap(t => parseMsg(t.value()))

    msgs.foreachRDD(rdd => rdd.saveToCassandra("poc", "users", AllColumns))

    ssc.start()
    ssc.awaitTermination()
  }

  def parseMsg(msg: String): Option[(Int, String, String, String)] = {
    try {
      val jsonParser = new JsonParser()
      val json = jsonParser.parse(msg)
        .getAsJsonObject

      val id = json.get("id").getAsInt
      val name = json.get("name").getAsString
      val occupation = if (json.keySet().contains("occupation")) json.get("occupation").getAsString else ""
      val incomeType = json.get("income_type").getAsString

      Some((id, name, occupation, incomeType))
    } catch {
      case _: Exception => None
    }

  }
}
