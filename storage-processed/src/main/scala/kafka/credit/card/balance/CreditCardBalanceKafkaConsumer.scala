package kafka.credit.card.balance

import com.google.gson.JsonParser
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.{Seconds, StreamingContext}
import com.datastax.spark.connector._
import utils.utils._

object CreditCardBalanceKafkaConsumer {
  def main(args: Array[String]): Unit = {
    disableLogging()

    val appName = "credit-card-balance-consumer"
    val sc = getSparkContext(appName)
    val ssc = new StreamingContext(sc, Seconds(1))
    ssc.checkpoint(s"$appName-checkpoint")

    KafkaUtils
      .createDirectStream[String, String](ssc, PreferConsistent, Subscribe[String, String](List("credit_card_balance_topic").toSet, getConsumerProperties))
      .flatMap(t => parseMsg(t.value()))
      .foreachRDD(rdd => if (!rdd.isEmpty) rdd.saveToCassandra("poc", "credit_card_balance", AllColumns))

    ssc.start()
    ssc.awaitTermination()
  }

  def parseMsg(msg: String): Option[(Int, Float)] = {
    try {
      val jsonParser = new JsonParser()
      val json = jsonParser.parse(msg)
        .getAsJsonObject

      val id = json.get("id").getAsInt
      val balance = json.get("balance").getAsFloat

      Some((id, balance))
    } catch {
      case e: Exception =>
        e.printStackTrace()
        None
    }
  }
}
