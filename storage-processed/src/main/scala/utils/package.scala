package utils

import config._
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}

package object utils {
  val config: Settings.StorageSettings.type = Settings.StorageSettings

  def getConsumerProperties: Map[String, String] = {
    Map[String, String](ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG -> config.kafkaBootstrapServers,
      ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG -> classOf[StringDeserializer].getName,
      ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG -> classOf[StringDeserializer].getName,
      ConsumerConfig.GROUP_ID_CONFIG -> "credit-cards",
      ConsumerConfig.AUTO_OFFSET_RESET_CONFIG -> "latest"
    )
  }

  def getSparkContext(appName: String): SparkContext = {
    disableLogging()

    val conf = new SparkConf(true)
      .set("spark.cassandra.connection.host", config.cassandraHost)
      .set("spark.cassandra.auth.username", config.cassandraUsername)
      .set("spark.cassandra.auth.password", config.cassandraPassword)
      .setAppName(appName)
      .setMaster(config.master)

    SparkContext.getOrCreate(conf)
  }

  def disableLogging(): Unit = {
    Logger.getLogger("org").setLevel(Level.ERROR)
    Logger.getLogger("akka").setLevel(Level.ERROR)
  }
}
