package config

import com.typesafe.config.ConfigFactory

object Settings {
  private val config = ConfigFactory.load()

  object ProcessorConfig {
    private val processorConfig = config.getConfig("processor")

    // env
    lazy val master: String = processorConfig.getString("master")

    // kafka
    lazy val kafkaBootstrapServers: String = processorConfig.getString("kafkaBootstrapServers")
  }

}
