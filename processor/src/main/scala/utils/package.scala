import com.google.gson.JsonObject
import config.Settings
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.sql.streaming.{DataStreamWriter, OutputMode}
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}

package object utils {
  val config: Settings.ProcessorConfig.type = Settings.ProcessorConfig

  def initConsumerProperties: Map[String, String] = {
    Map[String, String](ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG -> "nzigic-ubuntu-bigdata-1:9092",
      ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG -> classOf[StringDeserializer].getName,
      ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG -> classOf[StringDeserializer].getName,
      ConsumerConfig.GROUP_ID_CONFIG -> "credit-cards",
      ConsumerConfig.AUTO_OFFSET_RESET_CONFIG -> "earliest"
    )
  }

  private def getSparkConf(appName: String): SparkConf = {
    new SparkConf()
      .setAppName(appName)
      .setMaster(config.master)
      .set("spark.sql.streaming.checkpointLocation", s"$appName-checkpoint")
  }

  def getSparkSession(appName: String): SparkSession = {
    SparkSession
      .builder()
      .config(getSparkConf(appName))
      .getOrCreate()
  }

  def createKafkaWriter(data: DataFrame, topic: String, outputMode: OutputMode = OutputMode.Update): DataStreamWriter[Row] = {
    data
      .writeStream
      .format("kafka")
      .outputMode(outputMode)
      .option("kafka.bootstrap.servers", config.kafkaBootstrapServers)
      .option("topic", topic)
  }

  def prepareDataForKafka(data: Dataset[(String, String)]): DataFrame = {
    data.selectExpr("CAST (_1 AS STRING) AS key", "CAST (_2 AS STRING) AS value")
  }

  def startWriter(writer: DataStreamWriter[Row]): Unit = {
    writer.start().awaitTermination()
  }

  def toJson(data: Map[String, String]): String = {
    val json = new JsonObject()
    for (item <- data) {
      json.addProperty(item._1, item._2)
    }

    json.toString
  }


  def disableLogging(): Unit = {
    Logger.getLogger("org").setLevel(Level.ERROR)
    Logger.getLogger("akka").setLevel(Level.ERROR)
  }
}
