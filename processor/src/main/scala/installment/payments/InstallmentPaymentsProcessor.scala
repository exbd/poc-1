package installment.payments

import com.google.gson.JsonParser
import org.apache.spark.sql.{Dataset, Row}
import org.apache.spark.sql.functions._
import utils._

import scala.collection.immutable.{HashMap, ListMap, SortedMap}

object InstallmentPaymentsProcessor {
  val appName = "installment-payments-processor"

  // column names
  private val occupationIncomeTypeColName = "occupation_income_type"
  private val idColName = "id"
  private val paysOnTimeColName = "pays_on_time"
  private val totalIncomeColName = "total_income"
  private val paysOnTimePercentageColName = "pays_on_time_percentage"

  private val columnMapping = Map(occupationIncomeTypeColName -> 0,
    paysOnTimePercentageColName -> 1)

  def main(args: Array[String]): Unit = {
    disableLogging()

    val spark = getSparkSession(appName)
    import spark.implicits._

    val installmentPayments = spark
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", config.kafkaBootstrapServers)
      .option("subscribe", "installment_payments_topic")
      .load()
      .selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
      .as[(String, String)]

    val parsed = parseData(installmentPayments)
    val kafkaData = prepareDataForKafka(parsed)
    val writer = createKafkaWriter(kafkaData, "installment_payments_processed_topic")

    startWriter(writer)
  }

  def parseData(installmentPayments: Dataset[(String, String)]): Dataset[(String, String)] = {
    val sparkSession = getSparkSession(appName)
    import sparkSession.implicits._

    installmentPayments
      .flatMap(row => {
        try {
          val parser = new JsonParser()
          val installmentPayment = parser.parse(row._2).getAsJsonObject

          val id = installmentPayment.get(idColName).getAsInt
          val installmentAmount = installmentPayment.get("installment_amount").getAsDouble
          val paymentAmount = installmentPayment.get("payment_amount").getAsDouble
          val incomeType = installmentPayment.get("income_type").getAsString
          val occupationType = Option(installmentPayment.get("occupation_type").getAsString).filter(_.trim.nonEmpty)
          val totalIncome = installmentPayment.get("total_income").getAsDouble
          val housing = installmentPayment.get("housing_type").getAsString

          val occupationIncomeType = s"${occupationType.getOrElse(incomeType)} - $housing"
          val paysOnTime = installmentAmount <= paymentAmount

          print(s"Occ: $occupationIncomeType -> pays on time: $paysOnTime")

          Some((id.toString, occupationIncomeType, paysOnTime, totalIncome))
        } catch {
          case ex: Exception =>
            ex.printStackTrace()
            None
        }
      })
      .withColumnRenamed("_1", idColName)
      .withColumnRenamed("_2", occupationIncomeTypeColName)
      .withColumnRenamed("_3", paysOnTimeColName)
      .withColumnRenamed("_4", totalIncomeColName)
      .groupBy(occupationIncomeTypeColName)
      .agg(((sum(col(paysOnTimeColName).cast("long")) / count(paysOnTimeColName)) * 100).alias(paysOnTimePercentageColName))
      .map(item => getJson(item))
  }

  def getJson(item: Row): (String, String) = {
    val idIdx = columnMapping(occupationIncomeTypeColName)
    (item(idIdx).toString, toJson(mapColumns(item)))
  }

  def mapColumns(item: Row): Map[String, String] = {
    var result = new ListMap[String, String]
    for (col <- columnMapping) {
      result += (col._1 -> item(col._2).toString)
    }

    result
  }
}
